﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;

namespace T9_Spelling.Tests
{
    [TestClass]
    public class T9SpellingTests
    {
        [TestMethod]
        public void Test_a_to_2()
        {
            MessageTranslator mt = new MessageTranslator();
            string line = "a";
            string expected = "2";

            string actual = mt.LineToDigits(line);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test_def_to_3_33_333()
        {
            MessageTranslator mt = new MessageTranslator();
            string line = "def";
            string expected = "3 33 333";

            string actual = mt.LineToDigits(line);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test_message_to_6337777_77772433()
        {
            MessageTranslator mt = new MessageTranslator();
            string line = "message";
            string expected = "6337777 77772433";

            string actual = mt.LineToDigits(line);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestTranslate()
        {
            MessageTranslator mt = new MessageTranslator();
            using (StreamWriter writer = new StreamWriter("test.in"))
            {
                writer.WriteLine("1");
                writer.WriteLine("message");
            }
            string expected = "Case #1: 6337777 77772433";

            mt.Translate("test.in", "test.out");

            string actual;
            using (StreamReader reader = new StreamReader("test.out"))
            {
                actual = reader.ReadLine();
            }

            Assert.AreEqual(expected, actual);
        }
    }
}
