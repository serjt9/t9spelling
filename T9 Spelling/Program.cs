﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace T9_Spelling
{
    class Program
    {
        public static void Main(string[] args)
        {
            MessageTranslator MT = new MessageTranslator();
            MT.Translate("C-small-practice.in", "C-small-practice.out");
            MT.Translate("C-large-practice.in", "C-large-practice.out");
        }
    }
}
