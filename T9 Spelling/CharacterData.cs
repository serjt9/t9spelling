﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace T9_Spelling
{
    public struct CharacterData
    {
        public int digitButton;
        public string mappedDigits;

        public CharacterData(int digitButton, string mappedDigits)
        {
            this.digitButton = digitButton;
            this.mappedDigits = mappedDigits;
        }
    }
}
