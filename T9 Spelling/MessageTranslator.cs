﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace T9_Spelling
{
    public class MessageTranslator
    {
        private Dictionary<int, CharacterData> CharactersData = new Dictionary<int, CharacterData>();

        private void InitCharactersData()
        {
            CharactersData.Add(' ', new CharacterData(0, "0"));
            int digitButton = 2;
            string mappedDigits = "";
            int i = 0;
            int charsOnButton = 3;
            for (Char ch = 'a'; ch <= 'z'; ch++)
            {
                mappedDigits += digitButton;
                CharactersData.Add(ch, new CharacterData(digitButton, mappedDigits));
                i++;
                if (i == charsOnButton)
                {
                    digitButton++;
                    mappedDigits = "";
                    i = 0;
                    if (digitButton == 7 || digitButton == 9)
                        charsOnButton = 4;
                    else
                        charsOnButton = 3;
                }
            }
        }

        public string LineToDigits(string line)
        {
            int? lastButton = null;
            string digits = "";
            for (int i = 0; i < line.Length; i++)
            {
                Char ch = line[i];
                CharacterData chData = CharactersData[ch];
                if (lastButton == chData.digitButton)
                    digits += " ";
                lastButton = chData.digitButton;
                digits += chData.mappedDigits;
            }
            return digits;
        }

        public void Translate(string input, string output)
        {
            using (StreamReader reader = new StreamReader(input))
            {
                using (StreamWriter writer = new StreamWriter(output))
                {
                    int N;
                    if (int.TryParse(reader.ReadLine(), out N) && N >= 1 && N <= 100)
                    {
                        string line;
                        for (int i = 1; i <= N; i++)
                        {
                            line = reader.ReadLine();
                            writer.WriteLine("Case #" + i + ": " + LineToDigits(line));
                        }
                    }
                    else
                        Console.WriteLine("Incorrect number of messages.");

                }
            }
        }

        public MessageTranslator()
        {
            InitCharactersData();
        }
    }
}
